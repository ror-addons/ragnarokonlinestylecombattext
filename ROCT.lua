--[[
	Author: Stefan Rossbach, srossbach@arcor.de

	Edited by: Nick Fiala, Utakata (WHA) Peanut (Magnus)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

ScrollingCombatText = {}

local IncomingQueue;
local OutgoingQueue;

local IncomingLabels;
local OutgoingLabels;

local CurrentIncomingDisplays;
local CurrentOutgoingDisplays;

local NUM_LABELS = 15;

local ScreenDx, ScreenDy, OffsetY, Radius;
local c_RADIUS   = 75;
local c_OFFSET_Y = 50;

local LastIncomingDispatch, LastOutgoingDispatch;

local ColorIncomingHeal = {r = 0, b = 0,   g = 170};
local ColorOutgoingHeal = {r = 0, b = 0, g = 170};
local CritColor = {r = 255, b = 0, g = 255};


function ScrollingCombatText.Initialize()

	IncomingQueue = MicQueue(200);
	OutgoingQueue = MicQueue(200);

	CurrentIncomingDisplays = MicQueue(200);
	CurrentOutgoingDisplays = MicQueue(200);

	IncomingLabels = MicStack();
	OutgoingLabels = MicStack();

	local incoming, incomingText;
	local outgoing, outgoingText;
	
	for idx = 1, NUM_LABELS do

		incoming = "SctIncomingWindow" .. idx;
		outgoing = "SctOutgoingWindow" .. idx;
		incomingText = incoming .. "Text";
		outgoingText = outgoing .. "Text";	

		CreateWindowFromTemplate(incoming, "SctIncomingWindow", "Root");
		CreateWindowFromTemplate(outgoing, "SctOutgoingWindow", "Root");
		
		WindowSetScale(incomingText, 0.75);
		WindowSetScale(outgoingText, 1.0);
		
		WindowSetShowing(incoming, false);
		WindowSetShowing(outgoing, false);
		
		WindowClearAnchors(incoming);
		WindowClearAnchors(outgoing);
		WindowClearAnchors(incomingText);
		WindowClearAnchors(outgoingText);
		
		WindowAddAnchor(incoming, "center", "Root", "right", 0, 0);
		WindowAddAnchor(outgoing, "center", "Root", "left",  0, 0);
		WindowAddAnchor(incomingText, "right", incoming, "right", 0, 0);
		WindowAddAnchor(outgoingText, "left",  outgoing,  "left",  0, 0);

		IncomingLabels:push(incoming);
		OutgoingLabels:push(outgoing);
	end

	LastIncomingDispatch, LastOutgoingDispatch = 0, 0;

	ScrollingCombatText.NormalizeCoordinates();

	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "ScrollingCombatText.OnEvent");
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "ScrollingCombatText.HideWorldTextWindow");
	RegisterEventHandler(SystemData.Events.RESOLUTION_CHANGED, "ScrollingCombatText.NormalizeCoordinates");

	TextLogAddEntry("Chat", 0, L"RO-style Combat Text");
	ScrollingCombatText.HideWorldTextWindow();

end

function ScrollingCombatText.Shutdown()

	UnregisterEventHandler(SystemData.Events.WORLD_OBJ_HEALTH_CHANGED, "ScrollingCombatText.OnEvent");
	UnregisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "ScrollingCombatText.HideWorldTextWindow");
	UnregisterEventHandler(SystemData.Events.RESOLUTION_CHANGED, "ScrollingCombatText.NormalizeCoordinates");

	local data;

	-- destroy all current used windows

	while not CurrentOutgoingDisplays:isEmpty() do
		data = CurrentOutgoingDisplays:popFront();
		DestroyWindow(data.window);
	end

	while not CurrentIncomingDisplays:isEmpty() do
		data = CurrentOutgoingDisplays:popFront();
		DestroyWindow(data.window);
	end

	-- destroy all not current used windows

	while not OutgoingLabels:isEmpty() do
		DestroyWindow(OutgoingLabels:pop());
	end

	while not IncomigLabels:isEmpty() do
		DestroyWindow(IncomigLabels:pop());
	end
end

function ScrollingCombatText.NormalizeCoordinates()

	ScreenDx, ScreenDy  = GetScreenResolution();
	uiScale = InterfaceCore.GetScale();

	ScreenDx, ScreenDy  = math.ceil(ScreenDx / uiScale / 2), math.ceil(ScreenDy / uiScale / 2);
	Radius  = math.ceil(c_RADIUS / uiScale);
	OffsetY = math.ceil(c_OFFSET_Y / uiScale);

end

function ScrollingCombatText.HideWorldTextWindow()
	
	if (EA_System_EventEntry == nil) then
		TextLogAddEntry("Chat", 0, L"SCT Error: Could not deactivate Mythics SCT !");
	else
		UnregisterEventHandler( SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "EA_System_EventText.AddCombatEventText");
	end
end

function ScrollingCombatText.OnEvent(objectId, value, valueType)
	
	-- d(L"Id:" .. objectId .. L" value:" .. value .. L" type:" .. valueType);

	if (objectId == 0) or (value == 0) then
		return;
	end
		
	local combatData = {}

	combatData.isHeal = false;
	
	combatData.critical = (valueType == GameData.CombatEvent.CRITICAL) or (valueType == GameData.CombatEvent.ABILITY_CRITICAL);

	if value > 0 then
		combatData.value    = value;
		combatData.damage   = false;
		
		if objectId == GameData.Player.worldObjNum then
			combatData.color = ColorIncomingHeal;
			IncomingQueue:pushBack(combatData);
		else
			combatData.isHeal = true;
			combatData.color    = ColorOutgoingHeal;
			OutgoingQueue:pushBack(combatData);
		end
	else
		combatData.value    = -value;
		combatData.damage   = true;

		if objectId == GameData.Player.worldObjNum then
			combatData.color = DefaultColor.COLOR_INCOMING_DAMAGE;
			IncomingQueue:pushBack(combatData);
		else
			if combatData.critical == true then
				combatData.color = CritColor;
			else
				combatData.color = DefaultColor.COLOR_OUTGOING_DAMAGE;
			end
			OutgoingQueue:pushBack(combatData);
		end
	end
end

local function formatSCTString(damage, sources, criticals, value)

	local sctString;
	local src = L"";

	if (value > 6000) then
		if (sources < 2) then
			return L"";
		end
	end

	if (value < -6000) then
		if (sources < 2) then
			return L"";
		end
	end
	
	sources = sources - criticals
	if sources == 1 and criticals == 0 then
		return L"" .. value; 
	elseif sources == 0 and criticals == 1 then
		return L"! " .. value .. L" CRIT !";
	end

	if (criticals > 0) then
		sctString = L"! " .. value;
	else
		sctString = L"" .. value;
	end

	if damage == true then
		if sources == 1 then
			src = L"";
		elseif sources > 1 then
			src = L" (" .. sources .. L" Hits";
		end
	else
		if sources == 1 then
			src = L"";
		elseif sources > 1 then
			src = L" (" .. sources .. L" Spells";
		end
	end

	sctString = sctString .. src;

	if criticals == 0 then
		return sctString .. L")";
	end

	if sources > 0 then
		if criticals == 1 then
			if sources == 1 then
				return sctString .. L" CRIT !";
			else
				return sctString .. L", 1 CRIT) !";
			end
		else
			if sources == 1 then
				return sctString .. L", (" .. criticals .. L" CRITS) !";
			else
				return sctString .. L", " .. criticals .. L" CRITS) !";
			end
		end
	end
end


local NEXT_DISPATCH_TIME = .4

function ScrollingCombatText.DispatchCombatData()

	-- outgoing dispatches !

	if CurrentOutgoingDisplays:empty() or (LastOutgoingDispatch >= NEXT_DISPATCH_TIME) then
		
		if not OutgoingLabels:empty() then
			LastOutgoingDispatch = 0;

			if not OutgoingQueue:empty() then
				
				local sources, criticals, damage, value = 0, 0, false, 0;
				local color, data;
				data   = OutgoingQueue:first()
				damage = data.damage;
				color  = data.color;

				-- try to accumulate as much data as we can;
				while not OutgoingQueue:empty() do
					
					data = OutgoingQueue:first();
					if (data.damage ~= damage) then
						break;
					end

					if data.critical == true then
						criticals = criticals + 1;
						data.color = CritColor;
						color = CritColor;
						if (data.isHeal == true) then
							data.color = ColorOutgoingHeal;
							color = ColorOutgoingHeal;
						end
					end

					value = value + data.value;
					sources = sources + 1;
					OutgoingQueue:popFront();
				end

				local labelData = {};
				local labelText;
				labelData.position = 90;
				labelData.lastPosition = 90;
				labelData.window = OutgoingLabels:pop();
		
				LabelSetText(labelData.window.."Text", formatSCTString(damage, sources, criticals, value));
				LabelSetTextColor(labelData.window.."Text", color.r, color.g, color.b);
				WindowSetShowing(labelData.window, true); 
				WindowSetOffsetFromParent(labelData.window, ScreenDx - 100, OffsetY + ScreenDy - Radius - 50);
				CurrentOutgoingDisplays:pushBack(labelData);
			end			
		end
	end 

	-- incoming dispatches ! do not use a loop !

	if CurrentIncomingDisplays:empty() or (LastIncomingDispatch >= NEXT_DISPATCH_TIME) then
		
		if not IncomingLabels:empty() then
			LastIncomingDispatch = 0;

			if not IncomingQueue:empty() then
				
				local sources, criticals, damage, value = 0, 0, false, 0;
				local color, data;
				data   = IncomingQueue:first()
				damage = data.damage;
				color  = data.color;
			
				-- try to accumulate as much data as we can;
				while not IncomingQueue:empty() do
					
					data = IncomingQueue:first();
					if (data.damage ~= damage) then
						break;
					end

					if data.critical == true then
						criticals = criticals + 1;
					end

					value = value + data.value;
					sources = sources + 1;
					IncomingQueue:popFront();
				end

				local labelData = {};
				local labelText;
				labelData.position = 90;
				labelData.lastPosition = 90;
				labelData.window = IncomingLabels:pop();
		
				LabelSetText(labelData.window.."Text", formatSCTString(damage, sources, criticals, value));
				LabelSetTextColor(labelData.window.."Text", color.r, color.g, color.b);
				WindowSetShowing(labelData.window, true); 
				WindowSetOffsetFromParent(labelData.window, ScreenDx + 110, OffsetY + ScreenDy - Radius + 110);
				CurrentIncomingDisplays:pushBack(labelData);
			end			
		end
	end 
end

function ScrollingCombatText.OnUpdate(timePassed)

	LastIncomingDispatch = LastIncomingDispatch + timePassed; 
	LastOutgoingDispatch = LastOutgoingDispatch + timePassed;

	ScrollingCombatText.DispatchCombatData();

	for idx = 1, CurrentOutgoingDisplays:size() do
		local labelData = CurrentOutgoingDisplays:popFront();
		labelData.position = labelData.position - 60 * timePassed;
		
		if math.abs(labelData.position - labelData.lastPosition) >= 1 then
			labelData.lastPosition = labelData.position; 
			
			if (labelData.position <= 0) then
				OutgoingLabels:push(labelData.window)
				WindowSetShowing(labelData.window, false);
				labelData = nil;
			else
				local x, y;
				x = -math.floor(math.cos(labelData.position * 0.028) * Radius)-200;
				y = math.floor(math.sin(labelData.position * 0.028) * Radius)+100;
				WindowSetOffsetFromParent(labelData.window, ScreenDx + x, OffsetY + ScreenDy - y);
				CurrentOutgoingDisplays:pushBack(labelData);
			end
		else
			CurrentOutgoingDisplays:pushBack(labelData);
		end
	end

	for idx = 1, CurrentIncomingDisplays:size() do
		local labelData = CurrentIncomingDisplays:popFront();
		labelData.position = labelData.position + 60 * timePassed;
		
		if math.abs(labelData.position - labelData.lastPosition) >= 1 then
			labelData.lastPosition = labelData.position; 
			
			if (labelData.position >= 270) then
				IncomingLabels:push(labelData.window)
				WindowSetShowing(labelData.window, false);
				labelData = nil;
			else
				local x, y;
				x = -math.floor(math.cos(labelData.position * 0.009) * Radius)+190; 
				y = math.floor(math.sin(labelData.position * 0.009) * Radius)-75; 
				WindowSetOffsetFromParent(labelData.window, ScreenDx + x, OffsetY + ScreenDy - y);
				CurrentIncomingDisplays:pushBack(labelData);
			end
		else
			CurrentIncomingDisplays:pushBack(labelData);
		end
	end
end