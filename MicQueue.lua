--[[
	Author: Stefan Rossbach, srossbach@arcor.de

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

MicQueue = MicClass(function(data, capacity, ...)

	data.elem		= {};
	data.capacity	= capacity or 100; 
	data.count		= 0;
	data.head		= 1;
	data.tail		= 0;
end) 

function MicQueue:pushBack(elem)

	if ((self.capacity - self.count) == 0) then
		return false;
	end

	self.tail = self.tail + 1;

	if (self.tail > self.capacity) then
		self.tail = 1;
	end

	self.count = self.count + 1;

	self.elem[self.tail] = elem;
	
	return true;

end

MicQueue.enqueue = MicQueue.pushBack;

function MicQueue:popFront()

	if (self.count == 0) then
		return nil;
	end

	local elem = self.elem[self.head];
	self.elem[self.head] = nil;	

	self.head = self.head + 1;

	if (self.head > self.capacity) then
		self.head = 1;
	end

	self.count = self.count - 1;

	return elem;

end

MicQueue.dequeue = MicQueue.popFront;

function MicQueue:first()

	return self.elem[self.head];

end

MicQueue.head = MicQueue.first;


function MicQueue:last()

	return self.elem[self.tail];

end

MicQueue.tail = MicQueue.last;

function MicQueue:size()

	return self.count;
	 
end

function MicQueue:empty()

	return self.count == 0;

end

function MicQueue:clear()

	for i = 1, self.capacity do
		self.elem[i] = nil;
	end

	self.count, self.head, self.tail = 0, 1, 0;

end



