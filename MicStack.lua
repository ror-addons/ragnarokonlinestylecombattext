--[[
	Author: Stefan Rossbach, srossbach@arcor.de

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

MicStack = MicClass(function(data, ...)

	data.elem = {};

end) 

function MicStack:pop()
	
	if (#self.elem == 0) then
		return nil;
	end
	return table.remove(self.elem);

end

function MicStack:push(elem)

	table.insert(self.elem, elem)

end

function MicStack:top()

	if (#self.elem == 0) then
		return nil;
	end

	return self.elem[#self.elem];

end

function MicStack:size()

	return #self.elem;

end

function MicStack:empty()

	return #self.elem == 0;

end

function MicStack:clear()


	for i = 1, #self.elem do
		table.remove(self.elem);
	end

end
