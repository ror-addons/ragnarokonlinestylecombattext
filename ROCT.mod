<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="RO-Style Combat Text" version="1.6" date="10/05/2008" >

		<Author name="Peanut (Magnus)" email="eternalpeanut@gmail.com" />
        
		<Description text="RO-Style Combat Text version 1.6." />

		<Dependencies>
			<Dependency name="EASystem_Utils" />
			<Dependency name="EASystem_EventText" /> 
		</Dependencies>

		<Files>
			<File name="MicClass.lua" />
			<File name="MicQueue.lua" />
			<File name="MicStack.lua" />
			<File name="ROCT.xml" />
			<File name="ROCT.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="ScrollingCombatText.Initialize" />
		</OnInitialize>

		<OnUpdate>
			<CallFunction name="ScrollingCombatText.OnUpdate" />
		</OnUpdate>

		<OnShutdown>
			<CallFunction name="ScrollingCombatText.Shutdown" />
		</OnShutdown>

	</UiMod>

</ModuleFile>    